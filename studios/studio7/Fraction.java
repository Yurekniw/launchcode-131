package studio7;

import cse131.ArgsProcessor;
import exercises6.CommonDivisior;

public class Fraction {
	int numer;
	int denom;
	
	public Fraction (int numer, int denom){
		this.numer= numer;
		this.denom= denom;
		
	}
	
	public Fraction addFrac(Fraction name){
		int newNumer= this.numer + name.numer;
		int newDenom= this.denom + name.denom;
		Fraction fracNew = new Fraction(newNumer,newDenom);
		return fracNew;
	}
	
	public Fraction multFract(Fraction name){
		int newNumer= this.numer * name.numer;
		int newDenom= this.denom * name.denom;
		Fraction fracNew = new Fraction(newNumer,newDenom);
		return fracNew;
	}
	
	public Fraction recipFract(){
		int newNumer= this.denom;
		int newDenom= this.numer;
		Fraction fracNew = new Fraction(newNumer,newDenom);
		return fracNew;
	}
	
	public Fraction simplify(){
		int GCD = CommonDivisior.GCD(this.numer, this.denom);
		
		if (GCD==1){
			return this;
		}
		int newNumer= this.numer / GCD;
		int newDenom= this.denom / GCD;
		Fraction fracNew = new Fraction(newNumer,newDenom);
		return fracNew.simplify();
		
			
	}
	
	
	@Override
	public String toString() {
		return numer + "/" + denom;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		Fraction frac1 = new Fraction(3, 5);
		
		Fraction frac2 = new Fraction(4, 7);
		
		Fraction frac3 = frac1.addFrac(frac2);
		System.out.println(frac3.toString());
		
		Fraction frac4 = frac1.multFract(frac2);
		System.out.println(frac4.toString());
		
		Fraction frac5 = frac1.recipFract();
		System.out.println(frac4.toString());
		
		Fraction frac6 = new Fraction(16,38);
		Fraction frac7= frac6.simplify();
		System.out.println(frac7.toString());
	}

}
