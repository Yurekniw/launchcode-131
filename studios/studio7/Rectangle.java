package studio7;

public class Rectangle {
	private String name;
	private double length;
	private double width;
	
	public Rectangle(String name, double length, double width){
		this.name=name;
		this.length=length;
		this.width=width;
		
	}
	public double area(){
		return this.length * this.width;
	}
	
	public void perimeter(){
		double perimeter = this.length*2 + this.width*2;
		System.out.println(perimeter);
	}
	
	public boolean square(){
		if (this.length == this.width){
			System.out.println(this.name + " is in fact a square");
			return true;
		}
		else {
			System.out.println(this.name + "is not a square");
			return false;
		}
	}
	
	public void areaCompare(Rectangle name){
		if (this.area() == name.area()){
			System.out.println(this.name + " has the same area as " + name.name);
		}
		else if (this.area() > name.area()){
			System.out.println(this.name + " is bigger");
		}
		else {
			System.out.println(name.name + " is bigger");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Rectangle rect1 = new Rectangle("rect1", 5, 4);
		Rectangle rect2 = new Rectangle("rect2", 5, 7);
		
		rect1.areaCompare(rect2);
		System.out.println(rect1.area());
	}

}
