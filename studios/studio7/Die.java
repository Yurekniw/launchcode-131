package studio7;

public class Die {
	private double sides;
	
	public Die (int sides){
		this.sides = sides;
		
	}
	
	public int roll(){
		return (int) Math.ceil(Math.random()*this.sides);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Die D1 = new Die(12);
		
		for (int i=0; i<50; i++){
			System.out.println(D1.roll());
		}
	}

}
