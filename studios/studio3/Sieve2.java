package studio3;

import cse131.ArgsProcessor;

public class Sieve2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		//Declare variables
				int length = ap.nextInt("What is the highest number we want to consider")+1;
				boolean[] grid = new boolean[length];
				
				for (int i=2; i<length; i++){
					grid[i]=true;	
				}
				

				for (int i=2; i<length; i++){
					if (grid[i]){
						for (int j=i+1; j<length; j++){
							if (grid[j]){
								grid[j]= j % i != 0;
							}
						}
					}
				}
				System.out.println("\n");
				for (int i=0; i<length; i++){
					if (grid[i]){
					System.out.print(i + ", ");
					}
				}
			
	}

}
