package studio3;

import cse131.ArgsProcessor;

public class sieve {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		//Declare variables
		int length = ap.nextInt("What is the highest number we want to consider");
		int[] grid = new int[length];
		
		//populate array
		for (int i=0; i < length; i++){
			grid[i]=i+2;
		}
		for (int i=0; i<length; i++){
			System.out.print(grid[i] + ", ");
		}
		
		for (int i=0; i<length; i++){
			if (grid[i]>0){
				for (int j=i+1; j<length; j++){
					grid[j]= grid[j] % grid[i] == 0 ? 0 : grid[j];
				}
			}
		}
		System.out.println("\n");
		for (int i=0; i<length; i++){
			if (grid[i]>0){
			System.out.print(grid[i] + ", ");
			}
		}

		
		
	}

}
