package studio2;

public class Gambler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int startAmount= 12;
		double winChance= 0.25;
		int winAmount= 15;
		int totalPlays= 50;
		double lossChance= 1.0 - winChance;
		double ruin= 0;
		int losses= 0;
		
		if (lossChance != winChance){
			ruin = ((Math.pow((lossChance/winChance), startAmount))-
					(Math.pow((lossChance/winChance), winAmount)))
					/(1-(Math.pow((lossChance/winChance), winAmount)));
		}
		else {
			ruin = 1- (startAmount/winAmount);
		}
		for (int i=1; i <= totalPlays; i++){
			int cash = startAmount;
			int rounds = 0;
			while ( cash < winAmount && cash > 0){
				rounds=rounds+1;
				if (Math.random() > lossChance){
					cash++;
				}
				else {
				cash= cash-1;
				}
				
			}
			boolean outcome= cash == 0;
			System.out.println("Simulation " + i + " :" + rounds + " rounds \t \t" + (outcome ? "Lose": "WIN"));
			losses = outcome ? losses + 1 : losses;
			
				
			}
		
		System.out.println("\nLosses: " + losses + "\t Simulations:" + totalPlays);
		System.out.println("Actual Ruin Rate: " + (double)losses/totalPlays
				+ "\t Projected Ruin Rate: " + ruin);
		}
			
				
	
	}


