package studio2;

import cse131.ArgsProcessor;

public class Pi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int darts=ap.nextInt("How many throws will we make?");
		int hits=0;
		for (int i=1; i < darts; i++){
			double x = Math.random();
			double y = Math.random();
			double length = Math.sqrt(Math.pow(Math.abs(.5-x),2) + Math.pow(Math.abs(.5-y),2));
			hits = length < 0.5 ? hits+1 : hits;
			}
		int misses = darts - hits;
		double piCalculated= ((double)hits/darts)/(Math.pow(0.5,2));
		System.out.println("Hits: " + hits + "\nMisses: " + misses);
		System.out.println("Pi is close to: " + piCalculated);
		}
	}


