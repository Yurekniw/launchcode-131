package studio4;

import java.awt.Color;

import sedgewick.StdDraw;

public class Flag {

	
	public static void main(String[] args) {
		StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);
		//
		//  Add code for your studio below here.
		//
        StdDraw.rectangle(0, 0, 1, 2.0/3);
        
        //Red Half
        StdDraw.setPenColor(new Color(216, 16, 43));
        StdDraw.filledRectangle(0, .15, .3, .15);
        
        //Blue half
        StdDraw.setPenColor(new Color(0, 63, 133));  
        StdDraw.filledRectangle(0, -0.15, .3, .15);
        
        //Blue Circle
        StdDraw.filledCircle(0.15, -0.075, 0.166);
        
        //Red Circle
        StdDraw.setPenColor(new Color(216, 16, 43));
        StdDraw.filledCircle(-0.15, 0.075, 0.166);
        
        
        StdDraw.setPenRadius(.1);
        StdDraw.setPenColor(Color.WHITE);
        StdDraw.circle(0, 0, .4);
        
        // black lines
        StdDraw.setPenColor();
        StdDraw.setPenRadius(.03);
        StdDraw.line(0.3, 0.4, 0.5, 0.15);
        
        
	}

}
