package studio5;

public class Methods {
	
	/**
	 * Implemented correctly
	 * @param x one thing to add
	 * @param y the other thing to add
	 * @return the sum
	 */
	public static int sum(int x, int y) {
		return x+y;
	}
	
	/**
	 * 
	 * @param x one factor
	 * @param y another factor
	 * @return the product of the factors
	 */
	public static int mpy(int x, int y) {
		return x*y;  // FIXME
	}
	
	public static double avg3(int x, int y, int z){
		return (x+y+z)/3.0;
	}
	
	public static double sumArray(double[] a){
		double sum = 0;
		for (int i=0; i<a.length; i++){
			sum = sum + a[i];
		}
		return sum;
		
	}
	public static double average(double[] a){
		
		return sumArray(a)/a.length;
		
	}
	public static String pig(String s){
		
		
		
		
		return s.substring(1) + s.substring(0,1) + "ay";
		
	}
	public static int square(int a){
		return a*a;
	}

}
