package arrays;
import cse131.ArgsProcessor;

public class Sorting {
	
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		// initalize values
		int size = 0;
		int sortCount = 0;
		int min = 0;
		int minIndex = 0;
		int sum=0;
		
		//get size
		do {
			size = ap.nextInt("what is the size of the collection");
		} while (size < 0);
		
		int max = ap.nextInt("what is the largest possible value we want");
		//initalize array
		int[] collection = new int [size];
		
		//ask user to build array
		for (int i=0; i<size; i++){
			//collection[i]=ap.nextInt("enter next number");
			collection[i]=(int) (Math.random()*max+1);
		}
		
		//int[] collection={4,8,2,6};
		//print unsorted list
		System.out.println("Unsorted:\n");
		for (int i=0; i<size-1; i++){
			System.out.print(collection[i]+",");
		}
		System.out.println(collection[size-1]);
				
		//loop to sort
		while (sortCount<size){
			min = collection[sortCount];
			minIndex = sortCount;
			for (int i=sortCount; i<size; i++){
				if (collection[i]<min){
					min = collection[i];
					minIndex = i;
				}
			}
			collection[minIndex]=collection[sortCount];
			collection[sortCount]=min;
			sortCount++;
		}
		
		for (int i=0; i<size; i++){
			sum += collection[i];
		}
		
		//print sorted list and other data
		System.out.println("\nSorted:");
		for (int i=0; i<size-1; i++){
			System.out.print(collection[i]+",");
		}
		System.out.println(collection[size-1]);
		
		System.out.println("\nMean: " + sum/size);
		
		if (size%2 == 0){
			System.out.println("Median: " + (collection[size/2]+collection[(size-1)/2])/2.0);
		}
		else{
			System.out.println("Median: " + collection[size/2]);
		}
		System.out.println("Min: " + collection[0]);
		System.out.println("Max: " + collection[size-1]);
		System.out.println("Range: " + (collection[size-1]-collection[0]));
	
	}

}
