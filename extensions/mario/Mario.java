package mario;

import cse131.ArgsProcessor;

public class Mario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		// initialize and get values form user
		int gridHeight = 5; //ap.nextInt("size of the grid");
		int gridWidth = 20;
		double median = (gridWidth);
		double mult = (double)gridHeight/gridWidth;
		double step = (((double)gridHeight-gridWidth)/gridHeight);
				
		String upDown = ap.nextString("decending (Y/N)");
		String leftRight = ap.nextString("start on the left (Y/N)");
		
	
	
			// loop to print 
		for (int i=0; i < gridHeight; i++){
			//System.out.println(median);
			for (int j=0; j < gridWidth; j++){
				
				// top left
				if (i+j <= median-1 && upDown.equals("Y") && leftRight.equals("Y")){
					System.out.print("#");
				}
				
				// top right
				else if (i <= j*mult && upDown.equals("Y") && leftRight.equals("N")){
					System.out.print("#");
				}
				
				//bottom left
				else if (i*mult >= j && upDown.equals("N") && leftRight.equals("Y")){
					System.out.print("#");
				}
				
				//bottom right
				else if (i+j >= median -1 && upDown.equals("N") && leftRight.equals("N")){
					System.out.print("#");
				}
				
				else {
				System.out.print(" ");	
				}
			}
			System.out.print("\n");
			median= (double)median + step;
		}
	}

}
