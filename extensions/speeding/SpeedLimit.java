package speeding;

import cse131.ArgsProcessor;

public class SpeedLimit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor proc = new ArgsProcessor(args);
		int speed = proc.nextInt("Do you know how fast you were going?");
		int limit = proc.nextInt("What is the speed limit");
		int speeding = speed - limit;
		int fine = (speed - limit >= 10) ? 50+ ((speed-limit-10)*10) : 0;		
		
		System.out.println("You reported a speed of " + speed + " MPH for a speed limit of " + limit + " MPH. You went " + speeding + " MPH over the speed limit. Your fine is $" + fine +".");
		
	}

}
