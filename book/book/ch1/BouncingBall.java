package book.ch1;
import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.*;
/*************************************************************************
 *  Compilation:  javac BouncingBall.java
 *  Execution:    java BouncingBall
 *  Dependencies: StdDraw.java
 *
 *  Implementation of a 2-d bouncing ball in the box from (-1, -1) to (1, 1).
 *
 *  % java BouncingBall
 *
 *************************************************************************/

public class BouncingBall { 
    public static void main(String[] args) {
    	
    	ArgsProcessor ap = new ArgsProcessor(args);
    	int pause = ap.nextInt("Enter pause time:");

        // set the scale of the coordinate system
        StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);

        // initial values
        double rx = 0.480, ry = 0.860;     // position
        double vx = 0.015, vy = 0.023;     // velocity
        double radius = 0.05;              // radius
        
        int r = 0;
    	int g = 0;
    	int b = 0;
    	

        // main animation loop
        while (true)  { 

            // bounce off wall according to law of elastic collision
            if (Math.abs(rx + vx) > 1.0 - radius) vx = -vx;
            if (Math.abs(ry + vy) > 1.0 - radius) vy = -vy;

            // update position
            rx = rx + vx; 
            ry = ry + vy; 

            // clear the background
            StdDraw.setPenColor(StdDraw.GRAY);
            StdDraw.filledSquare(0, 0, 1.0);
            //draw grid
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.line(-1, 0, 1, 0);
            StdDraw.line(0, -1, 0, 1);

            // draw ball on the screen
            
//            if (rx >= 0 && ry >= 0){
//            	StdDraw.setPenColor(StdDraw.RED); 
//            }
//            else if (rx >= 0 && ry < 0 ){
//            	StdDraw.setPenColor(StdDraw.BLUE);
//            }
//            else if (rx < 0 && ry < 0 ){
//            	StdDraw.setPenColor(StdDraw.GREEN);
//            }
//            else {
//            	StdDraw.setPenColor(StdDraw.YELLOW);
//            }
            
            if (Math.abs(rx + vx) > 1.0 - radius){
            	r = (int)(Math.random()*256);
            	g = (int)(Math.random()*256);
            	b = (int)(Math.random()*256);

            }
            if (Math.abs(ry + vy) > 1.0 - radius){
            	r = (int)(Math.random()*256);
            	g = (int)(Math.random()*256);
            	b = (int)(Math.random()*256);

            }
            Color c = new Color(r, g, b);
            
            StdDraw.setPenColor(c);
            
            StdDraw.filledCircle(rx, ry, radius); 

            // display and pause for 20 ms
            StdDraw.show(pause); 
        } 
    } 
} 
