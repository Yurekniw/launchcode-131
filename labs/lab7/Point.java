package lab7;

public class Point {
	private double X;
	private double Y;
	
	public Point(double x, double y) {
		X = x;
		Y = y;
	}
	
	public double getX(){
		return this.X;
	}
	
	public double getY(){
		return this.Y;
	}
	
	public Point plus(Vector vec2){
		double newX= this.X + vec2.getDeltaX();
		double newY= this.Y + vec2.getDeltaY();
		Point pointNew = new Point(newX, newY);
		return pointNew;
	}
	
	public Vector minus(Point point2){
		double newX= this.X - point2.X;
		double newY= this.Y - point2.Y;
		Vector vecNew = new Vector(newX, newY);
		return vecNew;
	}
	
	public double distance(Point point2){
		return Math.sqrt(Math.pow((this.X - point2.X), 2) + Math.pow((this.Y - point2.Y), 2));
	}
}
