package lab7;

public class Vector {
	private double deltaX;
	private double deltaY;
	
	public Vector(double deltaX, double deltaY) {
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}
	
	public double getDeltaX(){
		return this.deltaX;
	}
	
	public double getDeltaY(){
		return this.deltaY;
	}
	
	public Vector plus(Vector vec2){
		double newX= this.deltaX + vec2.deltaX;
		double newY= this.deltaY + vec2.deltaY;
		Vector vecNew = new Vector(newX, newY);
		return vecNew;
	}
	
	public Vector minus(Vector vec2){
		double newX= this.deltaX - vec2.deltaX;
		double newY= this.deltaY - vec2.deltaY;
		Vector vecNew = new Vector(newX, newY);
		return vecNew;
	}
	
	public double magnitude(){
		return Math.sqrt(deltaX*deltaX+deltaY*deltaY);
	}
	
	public Vector rescale(double a){
		if (deltaX == 0 && deltaY == 0){
			double newX=a;
			Vector vecNew = new Vector(newX, 0);
			return vecNew;
			
		}
		double newX= deltaX*a/Math.sqrt(deltaX*deltaX+deltaY*deltaY);
		double newY= deltaY*a/Math.sqrt(deltaX*deltaX+deltaY*deltaY);
		Vector vecNew = new Vector(newX, newY);
		return vecNew;
	}
	
	public Vector scale(double a){
		double newX= this.deltaX * a;
		double newY= this.deltaY * a;
		Vector vecNew = new Vector(newX, newY);
		return vecNew;
	}
	
	public Vector deflectX(){
		return new Vector(-deltaX, deltaY);
	}
	
	public Vector deflectY(){
		return new Vector(deltaX, -deltaY);
	}


}
