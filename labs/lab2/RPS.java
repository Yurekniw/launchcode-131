package lab2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		// initialize values
		int totalGames =  10000; //ap.nextInt("How many games should we play? (please choose an odd integer)");
		int p1Wins = 0;
		int p2Wins = 0;
		int p2Choice= 1;
		String p1Play;
		String p2Play;
		
		// simulate results
		for (int i = 0; i < totalGames; i++){
			
			// P1 chose rock, paper or scissors
			double p1Choice = Math.random();
			if (p1Choice <= 0.33) {
				p1Play = "Rock";
			}
			else if (p1Choice <= 0.66){
				p1Play = "Paper";
			}
			else {
				p1Play = "Scissors";
			}
			System.out.print("P1: " + p1Play + "\t\t");
			
			//P2 choose rock paper or scissors
			if (p2Choice % 3 == 0) {
				p2Play = "Scissors";
			}
			else if (p2Choice % 3 == 1){
				p2Play = "Rock";
			}
			else {
				p2Play = "Paper";
			}
			p2Choice++;
			System.out.print("P2: " + p2Play + "\n");
			
			//determine winner
			if (p1Play == p2Play) {
				System.out.println("The Result is a tie");
			}
			else if (p1Play == "Rock"){
				if (p2Play == "Paper"){
					System.out.println("P2 Wins!");
					p2Wins++;
				}
				else {
					System.out.println("P1 Wins!");
					p1Wins++;
				}
			}
			else if (p1Play == "Paper"){
				if (p2Play == "Scissors"){
					System.out.println("P2 Wins!");
					p2Wins++;
				}
				else {
					System.out.println("P1 Wins!");
					p1Wins++;
				}
				
			}
			else {
				if (p2Play == "Rock"){
					System.out.println("P2 Wins!");
					p2Wins++;
				}
				else {
					System.out.println("P1 Wins!");
					p1Wins++;
				}

			}
			System.out.print("\n");
		}
		System.out.println("Player 1 was victorious in " + p1Wins + "/" + totalGames + " games.");
		System.out.println("Player 2 was victorious in " + p2Wins + "/" + totalGames + " games.");
	}
}
