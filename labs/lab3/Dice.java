package lab3;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int D = 3;
		int T = 1000;
		//declare variables
//		int D = ap.nextInt("How many dice are we using?");
//		int T = ap.nextInt("How many throws are we making?");
		int[] Throw = new int[D];
		int[] sums = new int[6*D+1];
		
		
		//loop to obtain results
		for (int i=0; i < T; i++){
			int sum = 0;
			for (int j=0; j < D; j++){
				Throw[j]=(int) Math.floor((Math.random()*6+1));
				// System.out.print(Throw[j] + " ");
				sum = sum + Throw[j];
			}
			// System.out.print("\t Throw " + i + " sum = " + sum);
			if (Throw[0]==Throw[1] && Throw[1]==Throw[2]){
				sums[0]++;
			}
			sums[sum]++;
			// System.out.print("\n");
			
		}
		System.out.println("\nThe dice all came up the same " + sums[0] + "/" + T + " times");
		System.out.println("SUM \t Number of times sum was seen");
		for (int i=1; i < sums.length; i++){
			if (sums[i]>0){
				System.out.println(i + "\t\t\t" + sums[i]);
			}
			
		}
		
	}

}
