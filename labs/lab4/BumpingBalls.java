package lab4;

import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {
	
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int pause = 20; 
		double radius = 0.1; 
		int balls = ap.nextInt("enter number of balls");
		int iterations = ap.nextInt("how many seconds should we run for")*(1000/pause);
		
		// Matrix to hold ball data defined as [position][velocity][ball#]
		double[][][] juggle = new double[2][2][balls];
		int[][] shade = new int[3][balls];
		
        // set the scale of the coordinate system
        StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);

        // initial values
        for (int i=0; i<balls; i++){
        	juggle[0][0][i]= Math.round((Math.random()*2-1)*1000.0)/1000.0; // x position
        	juggle[1][0][i]= Math.round((Math.random()*2-1)*1000.0)/1000.0; // y position
        	juggle[0][1][i]= Math.round((Math.random()*2-1)*1000.0)/100000.0; // x velocity
        	juggle[1][1][i]= Math.round((Math.random()*2-1)*1000.0)/100000.0; // y velocity
        	shade[0][i]=(int) Math.floor(Math.random()*256);
        	shade[1][i]=(int) Math.floor(Math.random()*256);
        	shade[2][i]=(int) Math.floor(Math.random()*256);
        }
        
        for (int i=0; i<balls; i++){
        	System.out.println(juggle[0][0][i] + " " + juggle[1][0][i]);
        	System.out.println(juggle[0][1][i] + " " + juggle[1][1][i]);
        	System.out.println("\n");
        }
        
        // run simulation
        for (int i=0; i < iterations; i++){
        	
        	// bounce off walls
        	for (int j=0; j < balls; j++){ 
        		//Bounce X
        		if (Math.abs(juggle[0][0][j] + juggle[0][1][j]) > 1.0 - radius){
        			juggle[0][1][j]=-juggle[0][1][j];
        			if (juggle[0][1][j] > 0 && juggle[0][0][j] > 0){
        				juggle[0][1][j]=-juggle[0][1][j];
        			}
        			else if (juggle[0][1][j] < 0 && juggle[0][0][j] < 0){
        				juggle[0][1][j]=-juggle[0][1][j];
        			}
        			else{
        				shade[0][j]=(int) Math.floor(Math.random()*256);
        				shade[1][j]=(int) Math.floor(Math.random()*256);
        				shade[2][j]=(int) Math.floor(Math.random()*256);
        			}
        			
        		}
        		//Bounce Y
        		if (Math.abs(juggle[1][0][j] + juggle[1][1][j]) > 1.0 - radius){
        			juggle[1][1][j]=-juggle[1][1][j];
        			if (juggle[1][1][j] > 0 && juggle[1][0][j] > 0){
        				juggle[1][1][j]=-juggle[1][1][j];
        			}
        			else if (juggle[1][1][j] < 0 && juggle[1][0][j] < 0){
        				juggle[1][1][j]=-juggle[1][1][j];
        			}
        			else{
        				shade[0][j]=(int) Math.floor(Math.random()*256);
        				shade[1][j]=(int) Math.floor(Math.random()*256);
        				shade[2][j]=(int) Math.floor(Math.random()*256);
        			}
        		}
        	}
        	//collide
        	for (int j=0; j < balls; j++){
        		for (int k=j+1; k < balls; k++){
        			// check for hit
        			double x1 = juggle[0][0][j];
        			double x2 = juggle[0][0][k];
        			double y1 = juggle[1][0][j];
        			double y2 = juggle[1][0][k];
        			
        			double dist = Math.sqrt(Math.pow(Math.abs(x1-x2),2) + Math.pow(Math.abs(y1-y2),2));
        			
        			//swap velocities and change colors if hit
        			if (dist < radius* 2){
        				double vx1 = juggle[0][1][j];
        				double vx2 = juggle[0][1][k];
        				double vy1 = juggle[1][1][j];
        				double vy2 = juggle[1][1][k];
        				juggle[0][1][j]= vx2;
        				juggle[0][1][k]= vx1;
        				juggle[1][1][j]= vy2;
        				juggle[1][1][k]= vy1;
        				
        				//first ball
        				shade[0][j]=(int) Math.floor(Math.random()*256);
                    	shade[1][j]=(int) Math.floor(Math.random()*256);
                    	shade[2][j]=(int) Math.floor(Math.random()*256);
                    	
                    	//second ball
                    	shade[0][k]=(int) Math.floor(Math.random()*256);
                    	shade[1][k]=(int) Math.floor(Math.random()*256);
                    	shade[2][k]=(int) Math.floor(Math.random()*256);
        			}
        		}
        	}
        	
        	//Update positions
        	for (int j=0; j < balls; j++){
        		juggle[0][0][j]= juggle[0][0][j] + juggle[0][1][j];
        		juggle[1][0][j]= juggle[1][0][j] + juggle[1][1][j];
        	}
        	
        	// Draw picture
        	
        	// clear the background
            StdDraw.setPenColor(StdDraw.GRAY);
            StdDraw.filledSquare(0, 0, 1.0);
            
            //draw border
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius();
            StdDraw.square(0, 0, 1.0);
            
            //draw balls
            for (int j=0; j<balls; j++){
            	StdDraw.setPenColor(new Color(shade[0][j],shade[1][j],shade[2][j]));
            	StdDraw.filledCircle(juggle[0][0][j], juggle[1][0][j], radius); 
            }
            StdDraw.show(pause);
        }
        
	}
}
