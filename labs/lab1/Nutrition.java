package lab1;

import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		String name= "Snickers"; //ap.nextString("What is the name of the food?:");
		double carbs= 34.5; //ap.nextDouble("How many grams of carbs does it contain?:");
		double fat= 13.6; //ap.nextDouble("How many grams of fat does it contain?:");
		double protein= 4.3; //ap.nextDouble("How many grams of protein does it contain?:");
		double statedCals= 271; //ap.nextDouble("What are the stated cals for the item?:");
		double calsActual = (4.0*carbs)+(4.0*protein)+(9.0*fat);
		double calsFiber= Math.round((calsActual- statedCals)*10)/10.0;;
		double fiber= Math.round((calsFiber/4.0)*100)/100.0;
		double percentFat=Math.round(((fat*9.0)/statedCals)*1000.0)/10.0;
		double percentCarbs=Math.round(((carbs*4.0)/statedCals)*1000.0)/10.0;
		double percentProtein=Math.round(((protein*4.0)/statedCals)*1000.0)/10.0;
		boolean lowCarb = percentCarbs < 25.0;
		boolean lowFat = percentFat < 15.0;
		boolean flip = 0.5 < Math.random();
		System.out.println("This food is said to have " + statedCals + "(available) Calories.");
		System.out.println("With " + calsFiber + " unavailable Calories, " + name + " has " + fiber + " grams of fiber.");
		System.out.println("\nApproximately \n \t" + percentCarbs +"% of your food is carbohydrates \n \t" + percentFat + "% of your food is fat \n \t" 
							+ percentProtein + "% of your food is protein");
		System.out.println("\nThis food is acceptable for a low-carb diet? " + lowCarb);
		System.out.println("This food is acceptable for a low-fat diet? " + lowFat);
		System.out.println("By coin flip, you should eat " + name + "? " + flip);
		
	}
}

