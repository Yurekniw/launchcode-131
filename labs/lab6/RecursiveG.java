package lab6;

public class RecursiveG {
	public static int gMethod (int x, int y){
		System.out.println(x + "," +y);
		if (x == 0){
			return y+1;
			}
		else if (x > 0 && y == 0){
			return gMethod(x-1,1);
		}
		return gMethod (x-1, gMethod(x, y-1));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(gMethod(3, 1));
	}

}
