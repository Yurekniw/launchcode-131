package lab6;

public class Beer {
	
	public static String bottlesOfBeer(int n){
			if (n == 1){
				return n + " bottles of beer on the wall, " + n 
						+ " bottles of beer; you take one down, pass it around, " + (n-1) 
						+ " bottles of beer on the wall.\n";
			}
			return n + " bottles of beer on the wall, " + n 
					+ " bottles of beer; you take one down, pass it around, " + (n-1) 
					+ " bottles of beer on the wall.\n" + bottlesOfBeer(n-1);
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(bottlesOfBeer(5));
	}
	

}
