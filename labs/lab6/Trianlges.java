package lab6;

import java.awt.Color;

import sedgewick.StdDraw;

public class Trianlges {
	
	public static void triangle(int n){
        double[] x = {0, 0.5, 1};
        double[] y = {0, Math.sqrt(3)/2, 0};
        StdDraw.filledPolygon(x, y);
        triMini(x, y, n);
	}
	public static void triMini(double[] x, double[] y, int n){
		if (n == 0){
			return;
		}
		double[] xflip=new double[3];
		double[] yflip=new double[3];
		
		for (int i=0; i<3; i++){
			
			xflip[i]=(x[(i+1)%3]+x[(i)%3])/2.0;
			yflip[i]=(y[(i+1)%3]+y[(i)%3])/2.0;
			}
			
		StdDraw.setPenColor(Color.WHITE);
		StdDraw.filledPolygon(xflip, yflip);
		
		double[] x1={x[0], xflip[0], xflip[2]};
		double[] y1={y[0], yflip[0], yflip[2]};
		double[] x2={x[1], xflip[1], xflip[0]};
		double[] y2={y[1], yflip[1], yflip[0]};
		double[] x3={x[2], xflip[1], xflip[2]};
		double[] y3={y[2], yflip[1], yflip[2]};
		
		triMini(x1, y1, n-1);
		triMini(x2, y2, n-1);
		triMini(x3, y3, n-1);
		}
	

		
		
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		triangle(6);
	}

}
