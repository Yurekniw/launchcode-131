package lab5;

public class Lab5Methods {
	public static int sumDownBy2(int n){
		int sum =0;
		while (n>0){
			sum += n;
			n-=2;
		}
		return sum;
		
	}
	public static double harmonicSum(int n){
		double sum =0.0;
		while(n > 0)
		{
			sum = sum + (1.0 / n);
			n--;
		}
		return sum;
		
	}
	public static double geometricSum(int n){
		double sum =0.0;
		while(n >= 0){
			sum += (1 / Math.pow(2, n));
			n--;
		}
		return sum;
	}
	public static int multPos(int j, int k){
		int sum = 0;
		for (int i=0; i<j; i++){
			sum += k;
		}
		return sum;
	}
	public static int mult(int j, int k){
		int sum = multPos(Math.abs(j),Math.abs(k));
		if (j < 0 ^ k < 0){
			sum= sum * -1;
		}
		return sum;
	}
	public static int expt(int n, int k){
		int sum = n;
		if (k == 0){
			sum=1;
			return sum;
		}
		if (k == 1){
			return sum;
		}
		
		for (int i=k; i>1; i--){
			sum *= n;
		}
		return sum;
	}
	

}
