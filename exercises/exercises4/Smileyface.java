package exercises4;

import java.awt.Color;

import sedgewick.StdDraw;

public class Smileyface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledCircle(0.5, 0.5, 0.5);
		StdDraw.setPenColor();
		StdDraw.circle(0.5, 0.5, 0.5);
		StdDraw.setPenRadius(0.125);
		StdDraw.point(0.25, 0.75);
		StdDraw.point(0.75, 0.75);
		StdDraw.setPenRadius(0.05);
		StdDraw.arc(0.5, 0.65, 0.4, 230, 310);
		for (int i=1; i<10; i++){
			
			while (!StdDraw.mousePressed()){
			}
			StdDraw.setPenRadius();
			if (i%2==0){
				StdDraw.setPenColor(Color.YELLOW);
			}
			else {
				StdDraw.setPenColor(Color.RED);
			}
			
			StdDraw.filledCircle(0.5, 0.5, 0.5);
			StdDraw.setPenColor();
			StdDraw.circle(0.5, 0.5, 0.5);
			StdDraw.setPenRadius(0.125);
			StdDraw.point(0.25, 0.75);
			StdDraw.point(0.75, 0.75);
			StdDraw.setPenRadius(0.05);
			StdDraw.arc(0.5, 0.65, 0.4, 230, 310);
			StdDraw.pause(100);
			
		}
		
	}

}
