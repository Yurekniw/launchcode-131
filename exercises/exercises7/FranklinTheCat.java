package exercises7;

public class FranklinTheCat {
	private String name;
	private int age;
	private int lives;
	private String furColor;
	private int fur;
	private double weight;
	
	public FranklinTheCat(String name, String firColor, double weight){
		this.age = 0;
		this.lives= 9;
		this.name= name;
		this.furColor = furColor;
		this.fur = 1000000;
		this.weight = weight;
		
	}
	
	public void increaseAge(){
		this.age=this.age + 1;
	}
	
	public void shed(){
		this.fur = this.fur - 1000;
		
	}
	
	public void shed(int fursLose){
		if (this.fur != 0){
			this.fur = this.fur - fursLose;
		}
		
	}
	
	public boolean	die(){
		if (this.lives != 0){
			this.lives=this.lives-1;
			return true;
		}
		else {
			return false;
		}
	}
	
	public void meow(){
		System.out.println("MEOW");
	}
	
	public void modWeight(double change){
		this.weight= this.weight + change;
	}
	
	public int compare(FranklinTheCat cat){
		return 0;
		
	}
	
	

	public static void main(String[] args) {
		FranklinTheCat cat1 = new FranklinTheCat("cat1", "orange", 5);
		FranklinTheCat cat2 = new FranklinTheCat("cat2", "white", 7);
	
		//cat1.compare(cat2);
		//cat2.compare(cat1);
		
		System.out.println(cat1.age);
		cat1.increaseAge();
		System.out.println(cat1.age);
		while (cat1.fur>0){
			cat1.shed(100000);
			System.out.println(cat1.fur);
		}
		
		while (cat1.lives > 0){
			System.out.println("cat1: ");
			cat1.meow();
			System.out.println("cat2: ");
			cat2.meow();
			cat1.die();
			
		}
		System.out.println("cat 1 has died of boredom");
		
	}

}
