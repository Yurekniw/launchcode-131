package exercises5;

import cse131.ArgsProcessor;

public class Reminder {

	public static int remainder(int a, int b) {
		// TODO Auto-generated method stub
		int goesInto = a/b;
		int rem = a-b*goesInto;
		
		return rem;
	}
	
	public static boolean isEven(int a) {
		if (remainder(a, 2)>0){
			return false;
		}
		else{
			return true;
		}

		
	}

}
