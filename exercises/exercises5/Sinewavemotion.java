package exercises5;

import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class Sinewavemotion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int points = ap.nextInt("How many points are we going to use?");
		double xval= 0;
		double yval= 0;
		double startval= 0;
		double dX = (2*Math.PI)/points;
		//draw grid
		StdDraw.setXscale(-2*Math.PI, 2*Math.PI);
        StdDraw.setYscale(-1.0, 1.0);
		StdDraw.filledRectangle(0, 0, 6.14, 1);
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.line(-6.28, 0, 6.28, 0);
		StdDraw.line(0, -1.0, 0, 1.0);
		
		while(true){
			StdDraw.clear(new Color(150, 190, 150));
		}
	}

}
