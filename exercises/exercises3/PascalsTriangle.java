package exercises3;

import cse131.ArgsProcessor;

public class PascalsTriangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int rows = ap.nextInt("rows");
		int columns = ap.nextInt("columns");
		
		int[][] pascal = new int[rows][columns];
		
		for (int i=0; i < rows; i++){
			for (int j=0; j < columns; j++){
				if(j == 0){
					pascal[i][j]=1;
				}
				else if(j == i){
					pascal[i][j]=1;
				}
				else if (i < 0 || j < 0 || i < j){
					pascal[i][j]=0;
				}
				else{
					pascal[i][j]=pascal[i-1][j]+pascal[i-1][j-1];
				}
			}
		}
		for (int i=0; i < rows; i++){
			for (int j=0; j < columns; j++){
				if (pascal[i][j] == 0){
					System.out.print("   ");
				}
				else {
					System.out.print(pascal[i][j] + " ");
				}
				
			}
			System.out.print("\n");
		}
		
	}

}
