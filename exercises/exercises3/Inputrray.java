package exercises3;

import cse131.ArgsProcessor;

public class Inputrray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int length = ap.nextInt("How many number are we storing?");
		
		int[] myArray = new int[length];
		
		// build array
		for (int i=0; i<length; i++){
			myArray[i] = ap.nextInt("enter an int");
		}
		
		//print array
		for (int i=0; i<length; i++){
			System.out.print(myArray[i] + " ");
		}
		//find largest value
		int large = myArray[0];
		for (int i=1; i<length; i++){
			large = large < myArray[i] ? myArray[i] : large;
		}
		System.out.println("\nThe largest value in the array is: " + large);
		
		//find smallest value
		int small = myArray[0];
		for (int i=1; i<length; i++){
			small = small > myArray[i] ? myArray[i] : small;
		}
		System.out.println("The smallest value in the array is: " + small);
		
		//average
		double ave = 0;
		for (int i=0; i<length; i++){
			ave = ave + myArray[i];
		}
		ave=ave/length;
		
		System.out.println("the average value of the array is: " + ave);
		
	}

}
