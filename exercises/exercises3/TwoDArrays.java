package exercises3;

public class TwoDArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[][] myArray = new double[3][3];
		double[] newArray = new double[3];
		
		//populate array
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				myArray[i][j]= (Math.round(Math.random()*1000))/10.0;
			}
		}
		
		//print array 
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				System.out.print(myArray[i][j] + "  ");
			}
			System.out.println("\n");
		}
		
		//fill second array
		double sum = 0;
		for (int j=0; j<3; j++){
			sum = 0;
			for (int i=0; i<3; i++){
				newArray[j] += myArray[i][j];
			}

		}
		System.out.println("\n");
		
		//print new array
		for (int j=0; j<3; j++){
			System.out.print(newArray[j] + "  ");
		}
	}

}
