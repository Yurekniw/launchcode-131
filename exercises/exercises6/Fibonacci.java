package exercises6;

public class Fibonacci {

	public static int[] fibonacci(int n) {
		int[] fib = new int[n];
		if (n>0){
			fib[0]=1;
		}
		if (n>1){
			fib[1]=1;
		}
		for (int i=2; i<n; i++){
			fib[i]=fib[i-1]+fib[i-2];
		}

		return fib;
	}

}
