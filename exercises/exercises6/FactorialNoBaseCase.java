package exercises6;

public class FactorialNoBaseCase {
	
	public static int factorial(int n) {
		int product = 1;
		for (int i=n; i>0; i--){
			product *= i;
		}
		return product;
	}

	public static void main(String[] args) {
		int ans = factorial(13);
		System.out.println("Answer is " + ans);
	}
}
