package exercises6;

import sedgewick.StdOut;

public class Mystery {

	public static int mystery(int a, int b) {
		if (b == 0) return 0;
		if (b % 2 == 0) return mystery(a+a, b/2);
		return mystery(a+a, b/2)+a;

	}
	public static void mystery1(int a, int b){
		if (a!=b){
			int m = (a+b)/2;
			mystery1(a,m);
			StdOut.print(m);
			mystery1(m,b);
		}
	}
	/*
	 * mystery (0,8)
	 * 	mystery (0,4)
	 * 		mystery (0,2)
	 * 			mystery(0,1)
	 * 				mystery(0,0)
	 * 			1
	 * 			
	 */
	
	public static void mystery2(int a, int b){
		if (a!=b){
			int m = (a+b)/2;
			mystery2(a,m-1);
			StdOut.print(m);
			mystery2(m+1,b);
		}
	}
	/*
	 * mystery2(0, 8)
	 * 		m=4
	 * 		mystery2(0, 3)
	 * 			m=1
	 * 			mystery2(0, 2)
	 * 				m=1
	 * 				mystery2(0, 0)
	 * 					return
	 * 				mystery2(1,0)
	 * 					m=0
	 * 					mystery2(0,0)
	 * 						return
	 * 					mystery2(1,0)
	 */
		
	public static void main(String[] args){
		mystery2(2,16);
	}
}
